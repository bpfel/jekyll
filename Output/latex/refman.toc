\contentsline {chapter}{\numberline {1}Main Page}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Installation}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Manjaro}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}others}{1}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Setup}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Snippets}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}neovim with Vundle}{2}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Snippets}{3}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Examples for functions and classes in Cpp}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Data Structure Index}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Data Structures}{5}{section.2.1}
\contentsline {chapter}{\numberline {3}File Index}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}File List}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}Data Structure Documentation}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Special\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Class Class Reference}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Constructor \& Destructor Documentation}{9}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Special\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Class()}{9}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Special\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Class()}{10}{subsubsection.4.1.2.2}
\contentsline {subsection}{\numberline {4.1.3}Member Function Documentation}{10}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}special\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Member\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Function()}{10}{subsubsection.4.1.3.1}
\contentsline {chapter}{\numberline {5}File Documentation}{11}{chapter.5}
\contentsline {section}{\numberline {5.1}Class.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{11}{section.5.1}
\contentsline {section}{\numberline {5.2}Doxygen.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{11}{section.5.2}
\contentsline {section}{\numberline {5.3}Function.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{11}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Function Documentation}{11}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}Pretty\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}print()}{11}{subsubsection.5.3.1.1}
\contentsline {subsubsection}{\numberline {5.3.1.2}Ugly\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}print()}{12}{subsubsection.5.3.1.2}
