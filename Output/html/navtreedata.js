var NAVTREE =
[
  [ "Doxygen for ARIS", "index.html", [
    [ "Installation", "index.html#Installation", [
      [ "Manjaro", "index.html#Linux", null ],
      [ "others", "index.html#All", null ]
    ] ],
    [ "Setup", "index.html#Setup", null ],
    [ "Snippets", "index.html#Snippets", [
      [ "neovim with Vundle", "index.html#Install", null ],
      [ "Snippets", "index.html#Using", null ]
    ] ],
    [ "Examples for functions and classes in Cpp", "index.html#Examples", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Class_8cpp.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';