/**
 * @brief This functions prints the word "Pretty".
 *
 * Remember to write functions names with underscores and a leading capital letter.
 */
void Pretty_print (void) {
	cout<<"Pretty"<<endl;
}

/**
 * @brief This is quite a useless function that returns how_ugly.
 *
 * Remember to write function names with underscores and a leading capital letter.
 *
 * @param how_ugly This is a parameter. Like a variable it is named with underscores.
 *
 * @return The return value of this function is how_ugly.
 */
int Ugly_print (int how_ugly) {
	for(int i = 0 ; i < how_ugly ; i++){
		cout<<"Ugly"<<endl;
	}
	return how_ugly;
}
