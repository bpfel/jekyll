 /**
  * @brief  This is the documentation of MySpecialClass.
  *
  * When naming classes always remember to use PascalCase.
  */
class MySpecialClass {

public:
	MySpecialClass();
	~MySpecialClass();
	/**
	 * @brief This is a small memberfunction
	 *
	 * When naming memberfunctions remember to use camelCase. Only the contructor and destructor are named in Pascal case, since they directly follow the class name.
	 *
	 * @param function_argument_1 Function arguments are handled like variables and are thus named with underscores.
	 * @param function_argument_2 
	 *
	 * @return Always tell people about the ouput of your function. This however should also be seen in the name of your function.
	 */
	int specialMemberFunction(int function_argument_1, float function_argument_2);
};
