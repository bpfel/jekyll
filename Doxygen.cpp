/**
 * \mainpage
 * 
 * This collection of code snippets shows how to apply doxygen documentation to cpp code.
 *
 * \tableofcontents
 * 
 * \section Installation
 * \subsection Linux Manjaro
 *
 * 1. sudo pacman -Su
 * 2. sudo pacman -S doxygen
 *
 * \subsection All others
 *
 * [Official Doxygen Documentation](https://www.stack.nl/~dimitri/doxygen/manual/install.html)
 *
 * \section Setup
 *
 * 1. `doxygen -g <cofig-file-name>`
 *	- This creates a configuration file, if you omit the file name, a file name Doxyfile will be created in the local folder.
 *
 * 2. Configure the doxyfile to your whishes. In this case the following changes have been made:
 * 	- `PROJECT_NAME = "Your Project Name"`
 * 	- `OUTPUT_DIRECTORY = "Your/Chosen/Directory"`
 * 	- `FULL_PATH_NAMES = NO` - Looks cleaner that way. Set to YES for code which has a sensible path hierarchy.
 * 	- `JAVADOC_AUTOBRIEF = YES` - So you don't need an explicit \brief tag.
 * 	- `HIDE_UNDOC_CLASSES = NO` - Let doxygen document everything.
 * 	- `GENERATE_LATEX = NO` - For now we use .html files.
 * 	- `TAB_SIZE = 4` - Default is 8.
 * 	- `OPTIMIZE_OUTPUT_FOR_C = YES` - For C and CPP code.
 * 	- `BUILTIN_STL_SUPPORT = YES`
 * 	- `EXTRACT_ALL = YES`
 * 	- `EXTRACT_LOCAL_CLASSES = YES`
 * 	- `RECURSIVE = YES` - Search subdirectories.
 * 	- `SOURCE_BROWSER = YES` - List source files.
 * 	- `ALPHABETICAL_INDEX = YES` - Generate alphabetic index of classes, structs and unions.
 * 	- `GENERATE_TREEVIEW = YES` - Generate side panel nav tree (html only).
 * 	- `TEMPLATE_RELATIONS = YES` - Show inheritance relationship for templates.
 * 	- `SEARCHENGINE = YES` - Creates local search facility.
 * 	- `FILE_PATTERNS = *.cpp *.h *.cc *.hpp` - specify file suffixes to document.
 * 	- `REFERENCED_BY_RELATION = YES` - document all entities associated with functions documented.
 * 	- `GENERATE_HTML = YES`
 * 	- `HTML_OUTPUT = html`
 * 	- `HTML_FILE_EXTENSION` = .html
 * 	- `HTML_ALIGN_MEMBERS` = YES
 * 	- `DISABLE_INDEX` = NO
 *
 * 3. `doxygen <config-file-name>`
 * 	- Calling doxygen in the directory of your source, with your configuration file as an argument will create the documentation.
 * 
 * 4. Check out your html documentation in the browser of your choice. To achieve that open the index.html file.
 *
 * \section Snippets
 *
 * \subsection Install neovim with Vundle
 *
 * Neovim is a powerful editor. Installing Vundle will allow us to enable a collection of plugins that simplify coding a lot.
 *
 * 	pacman -Su
 * 	pacman -S neovim
 * 	git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim
 * 	nvim ~/.config/nvim/init.vim
 *
 * Add the following lines to your 'init.vim':
 *
 * 	set nocompatible
 * 	filetype off
 *
 * 	"set the runtime path to include Vundle and initialize
 * 	set rtp+=~/.config/nvim/bundle/Vundle.vim
 * 	call vundle#begin("~/.config/nvim/bundle")
 * 	
 * 	Plugin 'VundleVim/Vundle.vim'
 * 	Plugin 'DoxygenToolkit.vim'
 * 	"here go any other plugins you'd like to add
 * 	call vundle#end()
 *	filetype plugin indent on
 *
 * After that close and save, reopen nvim and type `:PluginInstall`
 *
 * \subsection Using Snippets
 *
 * To use snippets go to the first line of any finished function or class and issue the command `:Dox`. The DoxygenToolkit then automatically inserts the corresponding syntax for your Doxygen Documentation.
 *
 *
 * \section Examples Examples for functions and classes in Cpp
 * 
 * First let's take a look at the documentation of functions:
 *
 * \verbinclude Function.cpp
 * 
 * To see what this results in consider: Function.cpp
 * 
 * In this next example you can see how classes are supposed to be documented:
 * 
 * \verbinclude Class.cpp
 *
 * To see what this results in consider: Class.cpp
 */
